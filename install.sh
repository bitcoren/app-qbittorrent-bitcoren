#!/usr/bin/env bash

cd $HRANILKA/apps/app-qbittorrent-bitcoren
mkdir data
wget -O qbittorrent-nox https://github.com/userdocs/qbittorrent-nox-static/releases/download/release-4.4.3.1_v1.2.16/x86_64-qbittorrent-nox
sudo DEBIAN_FRONTEND=noninteractive apt -y install qbittorrent-nox
sudo chmod +x qbittorrent-nox
sudo mv qbittorrent-nox /usr/bin/qbittorrent-nox
/usr/bin/qbittorrent-nox
sudo cp qbittorrent-nox.serv qbittorrent-nox.service
sed -i "s/User=qbittorrent/User=${USER}/g" qbittorrent-nox.service
sed -i "s/Group=qbittorrent/Group=${USER}/g" qbittorrent-nox.service
sudo mv qbittorrent-nox.service /etc/systemd/system/qbittorrent-nox.service
sudo systemctl daemon-reload
sudo systemctl enable qbittorrent-nox
sudo systemctl start qbittorrent-nox
